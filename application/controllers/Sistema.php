<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Sistema_model');
	}
	
	public function index()
	{
		$this->load->view('login');	
	}

	public function processarLogin()
	{		
		redirect('Sistema/dashboard');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Sistema');
	}

	public function dashboard()
	{		
		// v('auiii');
		// $dadosView['usuarios']  = '$this->Sistema_model->totalUsuarios()';
		// $dadosView['clientes']  = '$this->Sistema_model->totalClientes()';
		// $dadosView['vendas']    = $this->Sistema_model->totalVendas();		
		// $dadosView['pedidos']  = $this->Sistema_model->totalPedidos();

		$dadosView['meio'] = 'dashboard/dashboard';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function emitente()
	{
		$emitente = $this->input->post();

		if($emitente){

			unset($emitente['id']);

			$dados = array();

			foreach ($emitente as $key => $value) {
				$dados[$key] = $value;
			}
			
			$resultado = $this->Sistema_model->editarEmitente($dados,$this->input->post('id'));

			if($resultado){
	    		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionar o registro!');
	    	}

		}

		$dadosView['dados'] = $this->Sistema_model->pegarEmitente();
		$dadosView['meio'] = 'emitente/emitente';
		$this->load->view('tema/layout',$dadosView);
	}

	public function wscep()
    {
        $cep =  explode("-",$this->input->post('cep'));
        $cep = $cep[0].$cep[1];
        // $xml = file_get_contents('https://webservices.profissionaisdaweb.com.br/cep/'.$cep.'.xml');
        $xml = file_get_contents('https://viacep.com.br/ws/'.$cep.'/xml/');
        $xml = simplexml_load_string($xml);
        echo json_encode($xml);
    }

    public function selecionarCidade(){
		
		$estado  = $this->input->post('estado');
		$cidades = $this->Sistema_model->selecionarCidades($estado);

		echo  "<option value=''>Selecionar uma cidade</option>";
		foreach ($cidades as $cidade) {
			echo "<option value='".$cidade->nome."'>".$cidade->nome."</option>";
		}

	}

	public function pesquisa()
	{
		$pesquisa  = $this->input->post('pesquisa');
		$dadosView = '';
		$dadosView['clientes']  = $this->Sistema_model->pesquisarClientes($pesquisa);
		$dadosView['fornecedores'] = $this->Sistema_model->pesquisarfornecedores($pesquisa);

		$dadosView['meio'] = 'dashboard/pesquisa';
		$this->load->view('tema/layout',$dadosView);
	}

	public function backup()
	{
		$blakLinst = array('estados','tipo_combustivel','tipo_seguro','tipo_veiculo');

		$bkpcf = array(
		        'tables'        => array(),  
		        'ignore'        => $blakLinst,
		        'format'        => 'txt',                         
		        'filename'      => 'backup.'.date('d-m-Y').'.txt',                  
		        'add_drop'      => TRUE,                          
		        'add_insert'    => TRUE,
		        'newline'       => "\n"                           
		);

		// Carrega a classe DB utility 
		$this->load->dbutil();

		// Executa o backup do banco de dados armazenado-o em uma variável
		$backup = $this->dbutil->backup($bkpcf);
		
		// carrega o helper File e cria um arquivo com o conteúdo do backup
		$this->load->helper('file');
		write_file(FCPATH.'assets/arquivos/backups/backup.'.date('d-m-Y').'.txt', $backup);

		// Carrega o helper Download e força o download do arquivo que foi criado com 'write_file'
		force_download('backup.'.date('d-m-Y').'.txt', $backup);
	}

	public function alterarSenha()
	{

		$senha    = sha1(md5(strtolower($this->input->post('senha'))));	
		$novaSenha = sha1(md5(strtolower($this->input->post('novaSenha'))));

		if($senha != $this->session->userdata('usuario_senha')){
			$dadosView['msg']    = "Senha Atual não corresponde ao usuário logado!";
			$dadosView['status'] = false;
		}else{

			$dados['usuario_senha'] = $novaSenha;

			$resultado = $this->Sistema_model->alterarSenha($dados);

			if($resultado){
				$this->session->set_userdata('usuario_senha', $novaSenha);
	    		$dadosView['msg']    = "Senha alterada com Sucesso!";
				$dadosView['status'] = true;
	    	}else{
	    		$dadosView['msg']    = "Erro ao alterar a senha, tente mais tarde!";
				$dadosView['status'] = false;
	    	}
		}		

		echo json_encode($dadosView);

	}

	public function notificacoes()
	{	
		$dados['resultadoApi']  = $this->Sistema_model->apiEntrada();

		$resposta['api'] = $dados['resultadoApi'][0]->total;

		// $dados['resultadoTce']  = $this->Sistema_model->tcePendente();
		// $dados['resultadoTa'] = $this->Sistema_model->taPendente();


		// $resposta['ta'] = $dados['resultadoTa'][0]->total;
		// $resposta['tce'] = $dados['resultadoTce'][0]->total;

		echo json_encode($resposta);
	}


}
