<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Usuarios_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Usuarios_model->listar();

		$dadosView['meio'] = 'usuarios/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('usuario_cpf', 'CPF', 'trim|required|is_unique[usuarios.usuario_cpf]');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 				 
				  'usuario_cpf'                 => $this->input->post('usuario_cpf'),
				  'usuario_nome'                => $this->input->post('usuario_nome'),				 
				  'usuario_data_cadastro' 		=> date('Y-m-d'),
				  'usuario_email' 				=> $this->input->post('usuario_email'),				  
				  'usuario_senha' 		        => sha1(md5(strtolower($this->input->post('usuario_senha')))),
				  'usuario_ativo' 		        => $this->input->post('situacao'),	
				  'usuario_perfil'              => $this->input->post('usuario_perfil'),
        	);
        

        	$resultado = $this->Usuarios_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		$dadosView['perfil']   = $this->Usuarios_model->pegarPerfil();

		$dadosView['meio'] = 'usuarios/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('usuario_cpf', 'CPF', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 				 
				  'usuario_cpf'                 => $this->input->post('usuario_cpf'),
				  'usuario_nome'                => $this->input->post('usuario_nome'),
				  'usuario_email' 				=> $this->input->post('usuario_email'),	
				  'usuario_ativo' 		        => $this->input->post('situacao'),			  				 
				  'usuario_perfil'              => $this->input->post('usuario_perfil'),
        	);

        	if($this->input->post('usuario_senha')){
        		
        		$dados = array_merge($dados,['usuario_senha' => sha1(md5(strtolower($this->input->post('usuario_senha'))))]);
        	}
     
        	$resultado = $this->Usuarios_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Usuarios_model->listarId($this->uri->segment(3));
		$dadosView['perfil']   = $this->Usuarios_model->pegarPerfil();
		//$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		//$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->usuario_estado);
		$dadosView['meio']    = 'usuarios/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Usuarios_model->listarId($this->uri->segment(3));
		$dadosView['perfil']   = $this->Usuarios_model->pegarPerfil();
		//$dadosView['estados'] = $this->Usuarios_model->pegarEstados();
		//$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->usuario_estado);

		$dadosView['meio']  = 'usuarios/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'usuario_visivel' => 0					
					  );
		$resultado = $this->Usuarios_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Usuarios','refresh');
	}
}
