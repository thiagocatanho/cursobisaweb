<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <input disabled type="hidden" class="form-control" name="id" id="id"  value="<?php echo $dados[0]->usuario_id; ?>">
          <div class="box-body">

            <div class="form-group">
              <label for="usuario_cpf" class="col-sm-2 control-label">CPF</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" name="usuario_cpf" id="usuario_cpf"  value="<?php echo $dados[0]->usuario_cpf; ?>" placeholder="CPF">
              </div>
            </div>

            <div class="form-group">
              <label for="usuario_nome" class="col-sm-2 control-label">Nome Completo</label>
              <div class="col-sm-5">              
                <input disabled type="text" class="form-control" id="usuario_nome" name="usuario_nome"  value="<?php echo $dados[0]->usuario_nome; ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="usuario_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input disabled type="text" class="form-control" id="e-mail" name="usuario_email"  value="<?php echo $dados[0]->usuario_email; ?>" placeholder="E-mail">
              </div>
            </div>            

            <div class="form-group">
              <label for="usuario_perfil" class="col-sm-2 control-label">Perfil</label>
              <div class="col-sm-5">              
                
                <select disabled class="form-control" name="usuario_perfil" id="nivel">
                  <option value="">Selecione</option>
                  <?php foreach ($perfil as $valor) { ?>
                    <?php $selected = ($valor->perfil_id == $dados[0]->usuario_perfil)?'SELECTED':''; ?>
                    <option value='<?php echo $valor->perfil_id; ?>' <?php echo $selected; ?>><?php echo $valor->perfil_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>



          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>            
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>