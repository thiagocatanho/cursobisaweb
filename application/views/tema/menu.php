<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('usuario_nome'); ?> </p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('usuario_perfil_nome'); ?></a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>sistema/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>           
          </a>          
        </li>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-retweet"></i>
              <span>Menu</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Submenu</a></li>  
            </ul>                 

          </li>            
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(1));?>
        <small><?php echo ucfirst($this->uri->segment(2));?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>sistema/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url().$this->uri->segment(1);?>"><?php echo ucfirst($this->uri->segment(1));?></a></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(2));?></li>
      </ol>
    </section>

  <!-- Tratamento de mensagens do sitema -->
  <?php if($this->session->flashdata('error') or $this->session->flashdata('erro')){ ?>
      <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error') ; ?></h5>                
        </div>
      </div>
      <?php }else if($this->session->flashdata('success')){ ?>
      <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>               
        </div>
      </div>
      <?php } ?>